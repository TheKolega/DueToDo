# DueToDo
> A simple web-based ToDo list solution.

## Table of contents
- [General info](#general-info)
- [Features](#features)
- [Status](#status)
- [Technologies](#technologies)
- [Contact](#contact)

## General info
This is a web-based task management app offering users some categorization functionalities and a clean user interface to simplify usage.

## Features
- 👓 Users can have multiple boards, visible only to them, with an optional sharing feature.
- 📋 Each board can contain multiple tasks, which provides grouping as the user sees fit.
- ✅ A single task can belong to multiple boards, enabling more in-depth categorization.

## Status 
<div align="center">
	 🚧🔨🚧 <br> Work in progress
</div>

## Technologies
- Java: 15
- SpringBoot: 2.4.4
- React: -

## Contact
<a href="https://www.gitlab.com/TheKolega">
    <img src="https://img.shields.io/badge/GitLab-TheKolega-grey?style=for-the-badge&logo=GitLab&labelColor=548"/>
</a> &nbsp;
<a href="https://www.linkedin.com/in/milos-jov-rs">
    <img src="https://img.shields.io/badge/LinkedIn-Milo%C5%A1_Jovanovi%C4%87-grey?style=for-the-badge&logo=linkedin&labelColor=0A66C2"/>
</a>
