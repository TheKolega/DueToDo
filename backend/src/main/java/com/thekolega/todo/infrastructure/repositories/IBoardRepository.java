package com.thekolega.todo.infrastructure.repositories;

import com.thekolega.todo.domain.Board;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IBoardRepository extends JpaRepository<Board, Long> {

    public List<Board> findAllByUserId(Long userId);

    public Board findByUserIdAndId(Long userId, Long id);

}
