package com.thekolega.todo.infrastructure.repositories;

import com.thekolega.todo.domain.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ITaskRepository extends JpaRepository<Task, Long> {

    public List<Task> findAllDistinctByBoardsLinkedUserId(Long userId);

    public List<Task> findAllByBoardsLinkedUserIdAndBoardsLinkedId(Long userId, Long boardId);

    public Task findByBoardsLinkedUserIdAndBoardsLinkedIdAndId(Long userId, Long boardId, Long id);

    public Task findByBoardsLinkedUserIdAndId(Long userId, Long id);

}
