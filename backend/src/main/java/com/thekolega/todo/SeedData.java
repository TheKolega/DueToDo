package com.thekolega.todo;

import com.thekolega.todo.domain.Board;
import com.thekolega.todo.domain.Task;
import com.thekolega.todo.domain.User;
import com.thekolega.todo.infrastructure.repositories.IBoardRepository;
import com.thekolega.todo.infrastructure.repositories.ITaskRepository;
import com.thekolega.todo.infrastructure.repositories.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class SeedData {

    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private IBoardRepository boardRepository;
    @Autowired
    private ITaskRepository taskRepository;

    @PostConstruct
    public void seed() {
        var user1 = new User();
        user1.setUsername("U1");
        user1.setHashedPassword("spqr");
        user1.setFirstName("M");
        user1.setLastName("B");
        user1.setEmail("test1@test.rs");
        userRepository.save(user1);

        var user2 = new User();
        user2.setUsername("U2");
        user2.setHashedPassword("pzdnb");
        user2.setFirstName("M");
        user2.setLastName("J");
        user2.setEmail("test2@test.rs");
        userRepository.save(user2);

        var board1 = new Board();
        board1.setTitle(user1.getUsername() + "-B1");
        board1.setUser(user1);
        boardRepository.save(board1);

        var board2 = new Board();
        board2.setTitle(user2.getUsername() + "-B2");
        board2.setUser(user2);
        boardRepository.save(board2);

        var board3 = new Board();
        board3.setTitle(user1.getUsername() + "-B3");
        board3.setUser(user1);
        boardRepository.save(board3);

        var task1 = new Task();
        task1.setTitle(board1.getTitle() + "-T1");
        task1.setContent("Sadrzaj1");
        task1.linkBoard(board1);
        taskRepository.save(task1);

        var task2 = new Task();
        task2.setTitle(board1.getTitle() + "-T2");
        task2.setContent("Sadrzaj2");
        task2.linkBoard(board1);
        task2.linkBoard(board3);
        taskRepository.save(task2);

        var task3 = new Task();
        task3.setTitle(board3.getTitle() + "-T3");
        task3.setContent("Sadrzaj3");
        task3.linkBoard(board3);
        taskRepository.save(task3);

        var task4 = new Task();
        task4.setTitle(board2.getTitle() + "-T4");
        task4.setContent("Sadrzaj4");
        task4.linkBoard(board2);
        taskRepository.save(task4);

        boardRepository.save(board1);
        boardRepository.save(board2);
        boardRepository.save(board3);

    }

}
