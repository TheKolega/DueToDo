package com.thekolega.todo.domain;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Board {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    @Column(length = 64, nullable = false)
    private String title;

    @Setter(value = AccessLevel.NONE)
    @Column(updatable = false)
    private Timestamp timeCreated = new Timestamp(new Date().getTime());

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(name = "board_task",
            uniqueConstraints = {@UniqueConstraint(columnNames = {"boards_linked_id", "tasks_linked_id"})})
    private Set<Task> tasksLinked = new HashSet<>();

    @ManyToOne(fetch = FetchType.EAGER)
    private User user;

    public void linkTask(Task task) {
        task.getBoardsLinked().add(this);
        this.tasksLinked.add(task);
    }

    public void unlinkTask(Task task) {
        task.getBoardsLinked().remove(this);
        this.tasksLinked.remove(task);
    }

    public void setUser(User user) {
        this.user = user;
        if (!user.getBoardList().contains(this))
            user.getBoardList().add(this);
    }

}
