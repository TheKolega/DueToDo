package com.thekolega.todo.domain;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    @Column(length = 64, nullable = false)
    private String title;

    @Column(length = 256)
    private String content;

    @Setter(value = AccessLevel.NONE)
    @Column(updatable = false)
    private Timestamp timeCreated = new Timestamp(new Date().getTime());

    @Column(nullable = false)
    private boolean done = false;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "tasksLinked")
    private Set<Board> boardsLinked = new HashSet<>();

    public void linkBoard(Board board) {
        board.getTasksLinked().add(this);
        this.boardsLinked.add(board);
    }

    public void unlinkBoard(Board board) {
        board.getTasksLinked().remove(this);
        this.boardsLinked.remove(board);
    }

    public void prepareDelete() {
        for (var board : boardsLinked) {
            board.getTasksLinked().remove(this);
        }
    }

}
