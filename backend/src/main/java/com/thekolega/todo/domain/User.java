package com.thekolega.todo.domain;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    @Column(length = 64, nullable = false)
    private String username;

    @Column(length = 256)
    private String hashedPassword;

    @Column(length = 64)
    private String firstName;

    @Column(length = 64)
    private String lastName;

    @Column(length = 64)
    private String email;

    @Setter(value = AccessLevel.NONE)
    @Column(updatable = false)
    private Timestamp timeCreated = new Timestamp(new Date().getTime());

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.REMOVE)
    private List<Board> boardList = new ArrayList<>();

    public void addBoard(Board board) {
        if (!this.equals(board.getUser()))
            board.setUser(this);
        this.boardList.add(board);
    }

}
