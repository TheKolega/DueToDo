package com.thekolega.todo.application.services;

import com.thekolega.todo.domain.Board;

import java.util.List;

public interface IBoardService {

    public Board save(Board boardToSave);

    public List<Board> getAll(Long userId);

    public Board findById(Long userId, Long id);

    public Board delete(Long userId, Long id);

}
