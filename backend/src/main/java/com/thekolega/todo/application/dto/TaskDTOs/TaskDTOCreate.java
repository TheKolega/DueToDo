package com.thekolega.todo.application.dto.TaskDTOs;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
public class TaskDTOCreate {

    @NotBlank
    @Size(max = 64)
    private String title;

    @Size(max = 256)
    private String content;

    @JsonIgnore
    private Long userId;

    @JsonIgnore
    private Long boardId;

}
