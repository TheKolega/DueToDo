package com.thekolega.todo.application.services.implementation;

import com.thekolega.todo.application.services.IBoardService;
import com.thekolega.todo.application.services.ITaskService;
import com.thekolega.todo.domain.Board;
import com.thekolega.todo.domain.Task;
import com.thekolega.todo.infrastructure.repositories.IBoardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class BoardService implements IBoardService {

    @Autowired
    private IBoardRepository boardRepository;

    @Autowired
    private ITaskService taskService;

    @Override
    public Board save(Board boardToSave) {
        return boardRepository.save(boardToSave);
    }

    @Override
    public List<Board> getAll(Long userId) {
        return boardRepository.findAllByUserId(userId);
    }

    @Override
    public Board findById(Long userId, Long id) {
        return boardRepository.findByUserIdAndId(userId, id);
    }

    @Override
    public Board delete(Long userId, Long id) {
        var boardToDelete = this.findById(userId, id);
        if (boardToDelete != null) {
            var tasksLinked = copy(boardToDelete.getTasksLinked());
            for (var task : tasksLinked) {
                // Ako task pripada u vise boardova - unlink
                // Ako task pripada samo ovom boardu, cascade ce ga obrisati.
                if (task.getBoardsLinked().size() > 1) {
                    task.unlinkBoard(boardToDelete);
                }
            }
            boardRepository.delete(boardToDelete);
        }
        return boardToDelete;
    }

    private Set<Task> copy(Set<Task> tasks) {
        var copy = new HashSet<Task>();
        for (var task : tasks) {
            copy.add(task);
        }
        return copy;
    }

}
