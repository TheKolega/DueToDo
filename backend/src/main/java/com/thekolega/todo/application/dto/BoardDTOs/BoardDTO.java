package com.thekolega.todo.application.dto.BoardDTOs;

import com.thekolega.todo.application.dto.UserDTOs.UserDTO;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

@Getter
@Setter
public class BoardDTO {

    private Long id;

    private String title;

    private Timestamp timeCreated;

    private Long userId;

}
