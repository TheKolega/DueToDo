package com.thekolega.todo.application.dto.UserDTOs;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
public class UserDTOCreateOrEdit {

    private Long id;

    @NotBlank
    @Size(max = 64)
    private String username;

    @Size(max = 256)
    private String password;

    @Size(max = 256)
    private String passwordRepeat;

    @Size(max = 64)
    private String firstName;

    @Size(max = 64)
    private String lastName;

    @Size(max = 64)
    private String email;

}
