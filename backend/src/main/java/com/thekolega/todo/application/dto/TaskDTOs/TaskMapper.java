package com.thekolega.todo.application.dto.TaskDTOs;

import com.thekolega.todo.application.services.IBoardService;
import com.thekolega.todo.application.services.ITaskService;
import com.thekolega.todo.domain.Board;
import com.thekolega.todo.domain.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class TaskMapper {

    @Autowired
    ITaskService taskService;
    @Autowired
    private IBoardService boardService;

    public Task convertToEntity(TaskDTOCreate dto) {
        var entity = new Task();
        entity.setTitle(dto.getTitle());
        entity.setContent(dto.getContent());
        var board = boardService.findById(dto.getUserId(), dto.getBoardId());
        if (board == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Board with id: " + dto.getBoardId() + " not found.");
        }
        entity.linkBoard(board);
        return entity;
    }

    public Task convertToEntity(TaskDTOEdit dto) {
        var entity = taskService.findById(dto.getUserId(), dto.getBoardId(), dto.getId());
        if (entity == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Task with id: " + dto.getId() + "not found.");
        }
        entity.setTitle(dto.getTitle());
        entity.setContent(dto.getContent());
        entity.setDone(dto.isDone());
        return entity;
    }

    public TaskDTO convertToDTO(Task entity) {
        var dto = new TaskDTO();
        dto.setId(entity.getId());
        dto.setTitle(entity.getTitle());
        dto.setContent(entity.getContent());
        dto.setTimeCreated(entity.getTimeCreated());
        dto.setDone(entity.isDone());
        dto.setBoardIds(getBoardIds(entity));
        dto.setUserId(getUserId(entity));
        return dto;
    }

    public List<TaskDTO> convertToDTO(List<Task> entityList) {
        var dtoList = new ArrayList<TaskDTO>();
        for (var currentEntity : entityList) {
            var convertedDTO = convertToDTO(currentEntity);
            dtoList.add(convertedDTO);
        }
        return dtoList;
    }

    private List<Long> getBoardIds(Task entity) {
        return entity.getBoardsLinked().stream().map(Board::getId).collect(Collectors.toList());
    }

    private Long getUserId(Task entity) {
        var board = entity.getBoardsLinked().stream().findFirst().orElse(null);
        var user = board != null ? board.getUser() : null;
        return user != null ? user.getId() : null;
    }

}
