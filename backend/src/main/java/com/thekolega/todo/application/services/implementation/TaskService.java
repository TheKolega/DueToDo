package com.thekolega.todo.application.services.implementation;

import com.thekolega.todo.application.services.ITaskService;
import com.thekolega.todo.domain.Task;
import com.thekolega.todo.infrastructure.repositories.ITaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class TaskService implements ITaskService {

    @Autowired
    private ITaskRepository taskRepository;

    @Override
    public Task save(Task taskToSave) {
        return taskRepository.save(taskToSave);
    }

    @Override
    public List<Task> getAll(Long userId) {
        return taskRepository.findAllDistinctByBoardsLinkedUserId(userId);
    }

    @Override
    public List<Task> getAll(Long userId, Long boardId) {
        return taskRepository.findAllByBoardsLinkedUserIdAndBoardsLinkedId(userId, boardId);
    }

    @Override
    public Task findById(Long userId, Long boardId, Long id) {
        return taskRepository.findByBoardsLinkedUserIdAndBoardsLinkedIdAndId(userId, boardId, id);
    }

    @Override
    public Task delete(Long userId, Long id) {
        var taskToDelete = taskRepository.findByBoardsLinkedUserIdAndId(userId, id);
        if (taskToDelete != null) {
            taskToDelete.prepareDelete();
            taskRepository.delete(taskToDelete);
        }
        return taskToDelete;
    }

    @Override
    public Task delete(Long userId, Long boardId, Long id) {
        var taskToDelete = this.findById(userId, boardId, id);
        if (taskToDelete != null) {
            var board = taskToDelete.getBoardsLinked().stream().filter(x -> x.getId().equals(boardId)).findFirst().orElse(null);
            taskToDelete.unlinkBoard(board);
            if (taskToDelete.getBoardsLinked().size() == 0) {
                taskRepository.delete(taskToDelete);
            } else {
                taskRepository.save(taskToDelete);
            }
        }
        return taskToDelete;
    }

}
