package com.thekolega.todo.application.services.implementation;

import com.thekolega.todo.application.services.IBoardService;
import com.thekolega.todo.application.services.IUserService;
import com.thekolega.todo.domain.Board;
import com.thekolega.todo.domain.User;
import com.thekolega.todo.infrastructure.repositories.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements IUserService {

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IBoardService boardService;

    @Override
    public User create(User newUser) {
        newUser.setId(null);
        var savedUser = userRepository.save(newUser);
        var defaultBoard = createUserDefaultBoard(savedUser);
        boardService.save(defaultBoard);
        return savedUser;
    }

    @Override
    public User save(User userToSave) {
        return userRepository.save(userToSave);
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public User delete(Long id) {
        var userToDelete = this.findById(id);
        if (userToDelete != null)
            userRepository.delete(userToDelete);
        return userToDelete;
    }

    private Board createUserDefaultBoard(User user) {
        var board = new Board();
        board.setTitle("Default");
        board.setUser(user);
        return board;
    }

}
