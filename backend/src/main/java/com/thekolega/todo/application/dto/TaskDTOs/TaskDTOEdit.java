package com.thekolega.todo.application.dto.TaskDTOs;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class TaskDTOEdit {

    @NotBlank
    @Size(max = 64)
    private String title;

    @Size(max = 256)
    private String content;

    @NotNull
    private boolean done;

    @JsonIgnore
    private Long id;

    @JsonIgnore
    private Long boardId;

    @JsonIgnore
    private Long userId;

}
