package com.thekolega.todo.application.dto.BoardDTOs;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class BoardDTOCreate {

    @NotBlank
    @Size(max = 64)
    private String title = "Default";

    @JsonIgnore
    private Long userId;

}
