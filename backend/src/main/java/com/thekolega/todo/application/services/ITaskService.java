package com.thekolega.todo.application.services;

import com.thekolega.todo.domain.Task;

import java.util.List;

public interface ITaskService {

    public Task save(Task taskToSave);

    public List<Task> getAll(Long userId);

    public List<Task> getAll(Long userId, Long boardId);

    public Task findById(Long userId, Long boardId, Long id);

    public Task delete(Long userId, Long id);

    public Task delete(Long userId, Long boardId, Long id);

}
