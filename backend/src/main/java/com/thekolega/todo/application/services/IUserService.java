package com.thekolega.todo.application.services;

import com.thekolega.todo.domain.User;

import java.util.List;

public interface IUserService {

    public User create(User newUser);

    public User save(User userToSave);

    public List<User> getAll();

    public User findById(Long id);

    public User delete(Long id);

}
