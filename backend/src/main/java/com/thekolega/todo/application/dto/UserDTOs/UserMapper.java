package com.thekolega.todo.application.dto.UserDTOs;

import com.thekolega.todo.domain.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public User convertToEntity(UserDTOCreateOrEdit dto) {
        var entity = new User();
        entity.setId(dto.getId());
        entity.setUsername(dto.getUsername());
        entity.setFirstName(dto.getFirstName());
        entity.setLastName(dto.getLastName());
        entity.setEmail(dto.getEmail());
        entity.setHashedPassword(dto.getPassword()); //! kasnije namestiti hash
        return entity;
    }

    public UserDTO convertToDTO(User entity) {
        var dto = new UserDTO();
        dto.setId(entity.getId());
        dto.setUsername(entity.getUsername());
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());
        dto.setEmail(entity.getEmail());
        dto.setTimeCreated(entity.getTimeCreated());
        return dto;
    }

}
