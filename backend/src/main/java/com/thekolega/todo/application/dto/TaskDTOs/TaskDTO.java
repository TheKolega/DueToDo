package com.thekolega.todo.application.dto.TaskDTOs;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class TaskDTO {

    private Long id;

    private String title;

    private String content;

    private Timestamp timeCreated;

    private boolean done;

    private List<Long> boardIds = new ArrayList<>();

    private Long userId;

}
