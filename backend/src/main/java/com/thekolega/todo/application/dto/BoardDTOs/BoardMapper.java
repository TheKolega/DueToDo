package com.thekolega.todo.application.dto.BoardDTOs;

import com.thekolega.todo.application.dto.UserDTOs.UserMapper;
import com.thekolega.todo.application.services.IBoardService;
import com.thekolega.todo.application.services.IUserService;
import com.thekolega.todo.domain.Board;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@Component
public class BoardMapper {

    @Autowired
    private IUserService userService;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private IBoardService boardService;

    public Board convertToEntity(BoardDTOCreate dto) {
        var entity = new Board();
        entity.setTitle(dto.getTitle());
        var user = userService.findById(dto.getUserId());
        if (user == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User with id: " + dto.getUserId() + " not found.");
        }
        entity.setUser(user);
        return entity;
    }

    public Board convertToEntity(BoardDTOEdit dto) {
        var entity = boardService.findById(dto.getUserId(), dto.getId());
        if (entity == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Board with id: " + dto.getId() + " not found.");
        }
        entity.setTitle(dto.getTitle());
        return entity;
    }

    public BoardDTO convertToDTO(Board entity) {
        var dto = new BoardDTO();
        dto.setId(entity.getId());
        dto.setTitle(entity.getTitle());
        dto.setTimeCreated(entity.getTimeCreated());
        dto.setUserId(entity.getUser().getId());
        return dto;
    }

    public List<BoardDTO> convertToDTO(List<Board> entityList) {
        var dtoList = new ArrayList<BoardDTO>();
        for (var currentEntity : entityList) {
            var convertedDTO = convertToDTO(currentEntity);
            dtoList.add(convertedDTO);
        }
        return dtoList;
    }

}
