package com.thekolega.todo.api.controllers;

import com.thekolega.todo.application.dto.TaskDTOs.TaskDTO;
import com.thekolega.todo.application.dto.TaskDTOs.TaskDTOCreate;
import com.thekolega.todo.application.dto.TaskDTOs.TaskDTOEdit;
import com.thekolega.todo.application.dto.TaskDTOs.TaskMapper;
import com.thekolega.todo.application.services.ITaskService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/users/{userId}")
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private TaskMapper taskMapper;

    @GetMapping(value = "/tasks")
    public ResponseEntity<List<TaskDTO>> listAllTasksOfUser(@PathVariable Long userId) {
        var allTasks = taskService.getAll(userId);
        return new ResponseEntity<>(taskMapper.convertToDTO(allTasks), HttpStatus.OK);
    }

    @GetMapping(value = "/boards/{boardId}/tasks")
    public ResponseEntity<List<TaskDTO>> listAllTasksOfBoard(@PathVariable Long userId, @PathVariable Long boardId) {
        var allTasks = taskService.getAll(userId, boardId);
        return new ResponseEntity<>(taskMapper.convertToDTO(allTasks), HttpStatus.OK);
    }

    @GetMapping(value = "/boards/{boardId}/tasks/{id}")
    public ResponseEntity<TaskDTO> getTask(@PathVariable Long userId, @PathVariable Long boardId, @PathVariable Long id) {
        var gotTask = taskService.findById(userId, boardId, id);
        if (gotTask == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(taskMapper.convertToDTO(gotTask), HttpStatus.OK);
    }

    @PostMapping(value = "/boards/{boardId}/tasks", consumes = "application/json")
    public ResponseEntity<TaskDTO> createTask(@RequestBody TaskDTOCreate newTask, @PathVariable Long userId, @PathVariable Long boardId) {
        newTask.setUserId(userId);
        newTask.setBoardId(boardId);
        var convertedTask = taskMapper.convertToEntity(newTask);
        var savedTask = taskService.save(convertedTask);
        return new ResponseEntity<>(taskMapper.convertToDTO(savedTask), HttpStatus.CREATED);
    }

    @PutMapping(value = "/boards/{boardId}/tasks/{id}")
    public ResponseEntity<TaskDTO> editTask(@RequestBody TaskDTOEdit taskToEdit, @PathVariable Long userId, @PathVariable Long boardId, @PathVariable Long id) {
        taskToEdit.setUserId(userId);
        taskToEdit.setBoardId(boardId);
        taskToEdit.setId(id);
        var convertedTask = taskMapper.convertToEntity(taskToEdit);
        var editedTask = taskService.save(convertedTask);
        return new ResponseEntity<>(taskMapper.convertToDTO(editedTask), HttpStatus.OK);
    }

    @Operation(summary = "Fully delete task.")
    @DeleteMapping(value = "/tasks/{id}")
    public ResponseEntity<TaskDTO> deleteTask(@PathVariable Long userId, @PathVariable Long id) {
        var deletedTask = taskService.delete(userId, id);
        if (deletedTask == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(taskMapper.convertToDTO(deletedTask), HttpStatus.OK);
    }

    @Operation(summary = "Unlink task from board (delete if that is the only link).")
    @DeleteMapping(value = "/boards/{boardId}/tasks/{id}")
    public ResponseEntity<TaskDTO> deleteTask(@PathVariable Long userId, @PathVariable Long boardId, @PathVariable Long id) {
        var deletedTask = taskService.delete(userId, boardId, id);
        if (deletedTask == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(taskMapper.convertToDTO(deletedTask), HttpStatus.OK);
    }

}
