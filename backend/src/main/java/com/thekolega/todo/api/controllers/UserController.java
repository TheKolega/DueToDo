package com.thekolega.todo.api.controllers;

import com.thekolega.todo.application.dto.UserDTOs.UserDTO;
import com.thekolega.todo.application.dto.UserDTOs.UserDTOCreateOrEdit;
import com.thekolega.todo.application.dto.UserDTOs.UserMapper;
import com.thekolega.todo.application.services.IBoardService;
import com.thekolega.todo.application.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/users")
public class UserController {

    @Autowired
    private IUserService userService;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private IBoardService boardService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<UserDTO> getUser(@PathVariable Long id) {
        var gotUser = userService.findById(id);
        if (gotUser == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(userMapper.convertToDTO(gotUser), HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<UserDTO> createUser(@Valid @RequestBody UserDTOCreateOrEdit newUserDto) {
        checkPasswordMatchingOrThrow(newUserDto);
        var convertedUser = userMapper.convertToEntity(newUserDto);
        var savedUser = userService.create(convertedUser);
        return new ResponseEntity<>(userMapper.convertToDTO(savedUser), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<UserDTO> editUser(@RequestBody UserDTOCreateOrEdit newUser, @PathVariable Long id) {
        if (!id.equals(newUser.getId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Can't access data of another user.");
        }
        checkPasswordMatchingOrThrow(newUser);
        var userExists = userService.findById(id) != null;
        if (userExists) {
            var convertedUser = userMapper.convertToEntity(newUser);
            var editedUser = userService.save(convertedUser);
            return new ResponseEntity<>(userMapper.convertToDTO(editedUser), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<UserDTO> deleteUser(@PathVariable Long id) {
        var deletedUser = userService.delete(id);
        if (deletedUser == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(userMapper.convertToDTO(deletedUser), HttpStatus.OK);
    }

    private void checkPasswordMatchingOrThrow(@RequestBody UserDTOCreateOrEdit newUser) {
        if (!newUser.getPassword().equals(newUser.getPasswordRepeat())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Passwords don't match.");
        }
    }

}
