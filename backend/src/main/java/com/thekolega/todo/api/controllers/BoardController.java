package com.thekolega.todo.api.controllers;

import com.thekolega.todo.application.dto.BoardDTOs.BoardDTO;
import com.thekolega.todo.application.dto.BoardDTOs.BoardDTOCreate;
import com.thekolega.todo.application.dto.BoardDTOs.BoardDTOEdit;
import com.thekolega.todo.application.dto.BoardDTOs.BoardMapper;
import com.thekolega.todo.application.services.IBoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping(value = "/api/users/{userId}/boards")
public class BoardController {
    @Autowired
    private IBoardService boardService;

    @Autowired
    private BoardMapper boardMapper;

    @GetMapping(value = "/{id}")
    public ResponseEntity<BoardDTO> getBoard(@PathVariable Long userId, @PathVariable Long id) {
        var gotBoard = boardService.findById(userId, id);
        if (gotBoard == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(boardMapper.convertToDTO(gotBoard), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<BoardDTO>> listAllBoards(@PathVariable Long userId) {
        var allBoards = boardService.getAll(userId);
        return new ResponseEntity<>(boardMapper.convertToDTO(allBoards), HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<BoardDTO> createBoard(@RequestBody BoardDTOCreate newBoard, @PathVariable Long userId) {
        newBoard.setUserId(userId);
        var convertedBoard = boardMapper.convertToEntity(newBoard);
        var savedBoard = boardService.save(convertedBoard);
        return new ResponseEntity<>(boardMapper.convertToDTO(savedBoard), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<BoardDTO> editBoard(@RequestBody BoardDTOEdit newBoard, @PathVariable Long userId, @PathVariable Long id) {
        newBoard.setId(id);
        newBoard.setUserId(userId);
        var convertedBoard = boardMapper.convertToEntity(newBoard);
        var editedBoard = boardService.save(convertedBoard);
        return new ResponseEntity<>(boardMapper.convertToDTO(editedBoard), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<BoardDTO> deleteBoard(@PathVariable Long userId, @PathVariable Long id) {
        var deletedBoard = boardService.delete(userId, id);
        if (deletedBoard == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(boardMapper.convertToDTO(deletedBoard), HttpStatus.OK);
    }

}
